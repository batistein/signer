// nolint:revive
package design

import (
	"time"

	. "goa.design/goa/v3/dsl"
)

type CredentialDataCredentialProofRequest struct {
	Context           []string  `json:"@context"`
	Type              string    `json:"type"`
	Issuer            string    `json:"issuer"`
	IssuanceDate      time.Time `json:"issuanceDate"`
	CredentialSubject struct {
		Name  string `json:"name"`
		Allow bool   `json:"allow"`
	} `json:"credentialSubject"`
}

var CredentialProofRequest = Type("CredentialProofRequest", func() {
	Field(1, "namespace", String, "Key namespace.", func() {
		Example("transit")
	})
	Field(2, "key", String, "Key to use for the proof signature (optional).", func() {
		Example("key1")
	})
	Field(3, "credential", Any, "Verifiable Credential in JSON format.", func() {
		credentialDataCredentialProofRequest := &CredentialDataCredentialProofRequest{
			Context:      []string{"https://www.w3.org/2018/credentials/v1", "https://w3id.org/security/suites/jws-2020/v1", "https://schema.org"},
			Type:         "VerifiableCredential",
			Issuer:       "did:web:nginx:policy:policy:example:example:1.0:evaluation",
			IssuanceDate: time.Date(2010, 01, 01, 19, 23, 24, 651387237, time.UTC),
			CredentialSubject: struct {
				Name  string "json:\"name\""
				Allow bool   "json:\"allow\""
			}{Name: "Alice", Allow: true},
		}
		Example(credentialDataCredentialProofRequest)
	})
	Required("namespace", "key", "credential")
})

var PresentationProofRequest = Type("PresentationProofRequest", func() {
	Field(1, "issuer", String, "Issuer DID used to specify proof verification info.")
	Field(2, "namespace", String, "Key namespace.", func() {
		Example("transit")
	})
	Field(3, "key", String, "Key to use for the proof signature.", func() {
		Example("key1")
	})
	Field(4, "presentation", Any, "Verifiable Presentation in JSON format.")
	Required("issuer", "namespace", "key", "presentation")
})

var CreatePresentationRequest = Type("CreatePresentationRequest", func() {
	Field(1, "issuer", String, "Issuer DID of the Verifiable Presentation.", func() {
		Example("did:web:example.com")
	})
	Field(2, "namespace", String, "Key namespace.", func() {
		Example("transit")
	})
	Field(3, "key", String, "Key to use for the proof signature.", func() {
		Example("key1")
	})
	Field(4, "data", ArrayOf(Any), "Raw JSON to be included inside the VP as Verifiable Credential.", func() {
		Example([]map[string]interface{}{
			{"hello": "world"},
			{"hola": "mundo"},
		})
	})
	Field(5, "context", ArrayOf(String), "Additional JSONLD contexts to be specified in the VP.", func() {
		Example([]string{
			"https://w3id.org/security/suites/jws-2020/v1",
			"https://schema.org",
		})
	})
	Required("issuer", "namespace", "key", "data")
})

var VerifyCredentialRequest = Type("VerifyCredentialRequest", func() {
	Field(1, "credential", Bytes, "Verifiable Credential in JSON format.")
	Required("credential")
})

var VerifyPresentationRequest = Type("VerifyPresentationRequest", func() {
	Field(1, "presentation", Bytes, "Verifiable Presentation in JSON format.")
	Required("presentation")
})

var VerifyResult = Type("VerifyResult", func() {
	Field(1, "valid", Boolean, "Valid specifies if the proof is successfully verified.")
	Required("valid")
})

var NamespaceKeysRequest = Type("NamespaceKeysRequest", func() {
	Field(1, "namespace", String, "Namespace for signing keys.", func() {
		Example("did:web:example.com")
	})
	Required("namespace")
})

var VerificationMethodRequest = Type("VerificationMethodRequest", func() {
	Field(1, "namespace", String, "Key namespace.", func() {
		Example("transit")
	})
	Field(2, "key", String, "Name of requested key.", func() {
		Example("key1")
	})
	Field(3, "did", String, "DID controller of the key.", func() {
		Example("did:web:example.com")
	})
	Required("namespace", "key", "did")
})

var VerificationMethodsRequest = Type("VerificationMethodsRequest", func() {
	Field(1, "namespace", String, "Keys namespace.", func() {
		Example("transit")
	})
	Field(2, "did", String, "DID controller of the keys.", func() {
		Example("did:web:example.com")
	})
	Required("namespace", "did")
})

var DIDVerificationMethod = Type("DIDVerificationMethod", func() {
	Field(1, "id", String, "ID of verification method.", func() {
		Example("key1")
	})
	Field(2, "type", String, "Type of verification method key.", func() {
		Example("JsonWebKey2020")
	})
	Field(3, "controller", String, "Controller of verification method specified as DID.", func() {
		Example("did:web:example.com")
	})
	Field(4, "publicKeyJwk", Any, "Public Key encoded in JWK format.")
	Required("id", "type", "controller", "publicKeyJwk")
})
