package client

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
)

type Signer struct {
	addr string
}

func NewSigner(addr string) *Signer {
	return &Signer{addr: addr}
}

func (s *Signer) CreateCredentialProof(vc []byte) ([]byte, error) {
	var cred map[string]interface{}
	if err := json.Unmarshal(vc, &cred); err != nil {
		return nil, err
	}

	payload := map[string]interface{}{
		"namespace":  "transit",
		"key":        "key2",
		"credential": cred,
	}

	payloadJSON, err := json.Marshal(payload)
	if err != nil {
		return nil, err
	}

	req, err := http.NewRequest(http.MethodPost, s.addr+"/v1/credential/proof", bytes.NewReader(payloadJSON))
	if err != nil {
		return nil, err
	}

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return nil, newErrorResponse(resp)
	}

	return io.ReadAll(resp.Body)
}

func (s *Signer) VerifyCredentialProof(vc []byte) error {
	req, err := http.NewRequest(http.MethodPost, s.addr+"/v1/credential/verify", bytes.NewReader(vc))
	if err != nil {
		return err
	}

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return newErrorResponse(resp)
	}

	var response struct {
		Valid bool `json:"valid"`
	}
	dec := json.NewDecoder(resp.Body)
	dec.DisallowUnknownFields()
	if err := dec.Decode(&response); err != nil {
		return err
	}

	if !response.Valid {
		return fmt.Errorf("invalid credential")
	}

	return nil
}

func (s *Signer) CreatePresentationProof(vp []byte) ([]byte, error) {
	var pres map[string]interface{}
	if err := json.Unmarshal(vp, &pres); err != nil {
		return nil, err
	}

	payload := map[string]interface{}{
		"issuer":       "did:web:4db4-85-196-181-2.eu.ngrok.io:policy:policy:example:returnDID:1.0:evaluation",
		"namespace":    "transit",
		"key":          "key2",
		"presentation": pres,
	}

	payloadJSON, err := json.Marshal(payload)
	if err != nil {
		return nil, err
	}

	req, err := http.NewRequest(http.MethodPost, s.addr+"/v1/presentation/proof", bytes.NewReader(payloadJSON))
	if err != nil {
		return nil, err
	}

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return nil, newErrorResponse(resp)
	}

	return io.ReadAll(resp.Body)
}

func (s *Signer) VerifyPresentationProof(vp []byte) error {
	req, err := http.NewRequest(http.MethodPost, s.addr+"/v1/presentation/verify", bytes.NewReader(vp))
	if err != nil {
		return err
	}

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return newErrorResponse(resp)
	}

	var response struct {
		Valid bool `json:"valid"`
	}
	dec := json.NewDecoder(resp.Body)
	dec.DisallowUnknownFields()
	if err := dec.Decode(&response); err != nil {
		return err
	}

	if !response.Valid {
		return fmt.Errorf("invalid presentation")
	}

	return nil
}

func (s *Signer) CreatePresentation(data []byte) ([]byte, error) {
	req, err := http.NewRequest(http.MethodPost, s.addr+"/v1/presentation", bytes.NewReader(data))
	if err != nil {
		return nil, err
	}

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return nil, newErrorResponse(resp)
	}

	return io.ReadAll(resp.Body)
}

type errorResponse struct {
	Code     int
	Status   string
	Response string
}

func (e *errorResponse) Error() string {
	return fmt.Sprintf("Status: %s\nResponse: %s", e.Status, e.Response)
}

func newErrorResponse(resp *http.Response) *errorResponse {
	e := &errorResponse{Code: resp.StatusCode, Status: resp.Status}
	msg, err := io.ReadAll(resp.Body)
	if err == nil {
		e.Response = string(msg)
	}
	return e
}
