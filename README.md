[![pipeline status](https://gitlab.eclipse.org/eclipse/xfsc/tsa/signer/badges/main/pipeline.svg)](https://gitlab.eclipse.org/eclipse/xfsc/tsa/signer/-/commits/main)
[![coverage report](https://gitlab.eclipse.org/eclipse/xfsc/tsa/signer/badges/main/coverage.svg)](https://gitlab.eclipse.org/eclipse/xfsc/tsa/signer/-/commits/main)

# signer

Signer service exposes HTTP API to creates proofs for verifiable credentials 
and verifiable presentations. It exposes additional endpoints for retrieving 
public keys necessary for proofs verification.

It is developed using the [Goa v3](https://goa.design/) framework.

### Swagger Docs

[Swagger Web UI](./gen/http/openapi3.json)

In the local docker-compose environment a live Swagger UI is exposed at http://localhost:8085/swagger-ui/.

### High-level Overview

```mermaid  
flowchart LR
	A([client]) -- HTTP --> B[Signer API] 
	subgraph signer 
		B --HTTP--> C[Vault\nTransit API] 
		C --> D[(Vault)] 
	end
```
### Signing Key

The service uses Vault for making digital signatures on proofs. When a client
requests a proof, it can specify the name of the signing key to be used. If no
key is specified by the client, the signer service will use a default preconfigured
key name to make the proof. 

Default signing key name is specified in the ENV variable `VAULT_SIGNING_KEY`.

Default key or any other key which the clients specify to be used for signing,
*must* exist in the Vault, otherwise the request for proof will fail. The key must
also be of a supported type.

#### Supported key types

Supported key types are defined as ENV variable `VAULT_SUPPORTED_KEYS`. This
configuration exists because the Vault can contain different types of keys
some of which may not be supported by the codebase of the Signer service for
generating proofs. Example values:
```shell
VAULT_SUPPORTED_KEYS="ed25519,ecdsa-p256"
```

> Check out Hashicorp Vault docs for all [supported key types](https://www.vaultproject.io/api-docs/secret/transit#type-1) 
> by Vault Transit Engine. Keep in mind, that not all Vault key types may be supported 
> by the Aries framework signature suites and the signer service.

### Public Keys

The service exposes two endpoints for getting public keys - one for getting
a single key by name and the other for getting all possible public keys of
the signer service.

The keys are returned in JWK format and are wrapped in a DID Verification Method
envelope, so that the response can be used more easily during DID proofs verification
process. Example key response:

```json
{
  "id": "key1",
  "publicKeyJwk": {
    "crv": "P-256",
    "kid": "key1",
    "kty": "EC",
    "x": "RTx_2cyYcGVSIRP_826S32BiZxSgnzyXgRYmKP8N2l0",
    "y": "unnPzMAnbByBMq2l9WWKsDFE-MDvX6hYhrESsjAaT50"
  },
  "type": "JsonWebKey2020"
}
```

### Dependencies and Vendor

The project uses Go modules for managing dependencies and we commit the vendor directory.
When you add/change dependencies, be sure to clean and update the vendor directory before
submitting your Merge Request for review.

```shell
go mod tidy
go mod vendor
```

### Tests and Linters

To execute the units tests for the service go to the root project directory and run:

```shell
go test -race $(go list ./... | grep -v /integration)
```

To run the linters go to the root project directory and run:
```shell
golangci-lint run
```

### Integration Tests

Integration tests are inside the [integration](./integration) directory.
The only configuration option they need is the base URL of the signer service.
It must be specified in the `SIGNER_ADDR` environment variable. 
The tests can be executed against different environments by setting the
value for `SIGNER_ADDR`.

```shell
SIGNER_ADDR=https://{{SIGNER_ADDRESS}} go test
```

> Note: these tests are not executed in the CI pipeline currently.

## Dependencies

[Dependencies](go.mod)

## License

[Apache 2.0 license](LICENSE)
