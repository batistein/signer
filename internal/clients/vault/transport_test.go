package vault

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_lastPublicKeyVersion(t *testing.T) {
	tests := []struct {
		name string
		keys map[string]interface{}
		key  string
	}{
		{
			name: "no keys in response",
		},
		{
			name: "one key that doesn't contain public key",
			keys: map[string]interface{}{
				"1": 123456,
			},
			key: "",
		},
		{
			name: "one key in response",
			keys: map[string]interface{}{
				"1": map[string]interface{}{"public_key": "key1"},
			},
			key: "key1",
		},
		{
			name: "two keys in response",
			keys: map[string]interface{}{
				"1": map[string]interface{}{"public_key": "key1"},
				"2": map[string]interface{}{"public_key": "key2"},
			},
			key: "key2",
		},
		{
			name: "three keys in response",
			keys: map[string]interface{}{
				"2": map[string]interface{}{"public_key": "key1"},
				"1": map[string]interface{}{"public_key": "key2"},
				"4": map[string]interface{}{"public_key": "key4"},
			},
			key: "key4",
		},
		{
			name: "four keys in response",
			keys: map[string]interface{}{
				"2": map[string]interface{}{"public_key": "key1"},
				"1": map[string]interface{}{"public_key": "key2"},
				"4": map[string]interface{}{"public_key": "key4"},
				"8": map[string]interface{}{"public_key": "key8"},
			},
			key: "key8",
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			response := &getKeyResponse{}
			response.Data.Keys = test.keys
			key := response.lastPublicKeyVersion()
			assert.Equal(t, test.key, key)
		})
	}
}
