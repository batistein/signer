swagger: "2.0"
info:
    title: Signer Service
    description: Signer service exposes HTTP API for making and verifying digital signatures and proofs for Verifiable Credentials.
    version: ""
host: localhost:8085
consumes:
    - application/json
    - application/xml
    - application/gob
produces:
    - application/json
    - application/xml
    - application/gob
paths:
    /liveness:
        get:
            tags:
                - health
            summary: Liveness health
            operationId: health#Liveness
            responses:
                "200":
                    description: OK response.
            schemes:
                - http
    /readiness:
        get:
            tags:
                - health
            summary: Readiness health
            operationId: health#Readiness
            responses:
                "200":
                    description: OK response.
            schemes:
                - http
    /v1/credential/proof:
        post:
            tags:
                - signer
            summary: CredentialProof signer
            description: CredentialProof adds a proof to a given Verifiable Credential.
            operationId: signer#CredentialProof
            parameters:
                - name: CredentialProofRequestBody
                  in: body
                  required: true
                  schema:
                    $ref: '#/definitions/SignerCredentialProofRequestBody'
                    required:
                        - namespace
                        - key
                        - credential
            responses:
                "200":
                    description: OK response.
                    schema:
                        type: string
                        format: binary
            schemes:
                - http
    /v1/credential/verify:
        post:
            tags:
                - signer
            summary: VerifyCredential signer
            description: VerifyCredential verifies the proof of a Verifiable Credential.
            operationId: signer#VerifyCredential
            parameters:
                - name: bytes
                  in: body
                  description: Verifiable Credential in JSON format.
                  required: true
                  schema:
                    type: string
                    format: byte
            responses:
                "200":
                    description: OK response.
                    schema:
                        $ref: '#/definitions/SignerVerifyCredentialResponseBody'
                        required:
                            - valid
            schemes:
                - http
    /v1/namespaces:
        get:
            tags:
                - signer
            summary: Namespaces signer
            description: Namespaces returns all keys namespaces, which corresponds to enabled Vault transit engines.
            operationId: signer#Namespaces
            responses:
                "200":
                    description: OK response.
                    schema:
                        type: array
                        items:
                            type: string
                            example: Sed quos rerum deserunt a nihil aut.
            schemes:
                - http
    /v1/namespaces/{namespace}/keys:
        get:
            tags:
                - signer
            summary: NamespaceKeys signer
            description: NamespaceKeys returns all keys in a given namespace.
            operationId: signer#NamespaceKeys
            parameters:
                - name: namespace
                  in: path
                  description: Namespace for signing keys.
                  required: true
                  type: string
            responses:
                "200":
                    description: OK response.
                    schema:
                        type: array
                        items:
                            type: string
                            example: Qui laboriosam qui est.
            schemes:
                - http
    /v1/presentation:
        post:
            tags:
                - signer
            summary: CreatePresentation signer
            description: CreatePresentation creates VP with proof from raw JSON data.
            operationId: signer#CreatePresentation
            parameters:
                - name: CreatePresentationRequestBody
                  in: body
                  required: true
                  schema:
                    $ref: '#/definitions/SignerCreatePresentationRequestBody'
                    required:
                        - issuer
                        - namespace
                        - key
                        - data
            responses:
                "200":
                    description: OK response.
                    schema:
                        type: string
                        format: binary
            schemes:
                - http
    /v1/presentation/proof:
        post:
            tags:
                - signer
            summary: PresentationProof signer
            description: PresentationProof adds a proof to a given Verifiable Presentation.
            operationId: signer#PresentationProof
            parameters:
                - name: PresentationProofRequestBody
                  in: body
                  required: true
                  schema:
                    $ref: '#/definitions/SignerPresentationProofRequestBody'
                    required:
                        - issuer
                        - namespace
                        - key
                        - presentation
            responses:
                "200":
                    description: OK response.
                    schema:
                        type: string
                        format: binary
            schemes:
                - http
    /v1/presentation/verify:
        post:
            tags:
                - signer
            summary: VerifyPresentation signer
            description: VerifyPresentation verifies the proof of a Verifiable Presentation.
            operationId: signer#VerifyPresentation
            parameters:
                - name: bytes
                  in: body
                  description: Verifiable Presentation in JSON format.
                  required: true
                  schema:
                    type: string
                    format: byte
            responses:
                "200":
                    description: OK response.
                    schema:
                        $ref: '#/definitions/SignerVerifyPresentationResponseBody'
                        required:
                            - valid
            schemes:
                - http
    /v1/verification-methods/{namespace}/{did}:
        get:
            tags:
                - signer
            summary: VerificationMethods signer
            description: VerificationMethods returns all public keys in a given namespace. The result is formatted as array of DID verification methods with their controller attribute being the given DID in the request.
            operationId: signer#VerificationMethods
            parameters:
                - name: namespace
                  in: path
                  description: Keys namespace.
                  required: true
                  type: string
                - name: did
                  in: path
                  description: DID controller of the keys.
                  required: true
                  type: string
            responses:
                "200":
                    description: OK response.
                    schema:
                        type: array
                        items:
                            $ref: '#/definitions/DIDVerificationMethodResponse'
            schemes:
                - http
    /v1/verification-methods/{namespace}/{key}/{did}:
        get:
            tags:
                - signer
            summary: VerificationMethod signer
            description: VerificationMethod returns a single public key formatted as DID verification method for a given namespace, key and did.
            operationId: signer#VerificationMethod
            parameters:
                - name: namespace
                  in: path
                  description: Key namespace.
                  required: true
                  type: string
                - name: key
                  in: path
                  description: Name of requested key.
                  required: true
                  type: string
                - name: did
                  in: path
                  description: DID controller of the key.
                  required: true
                  type: string
            responses:
                "200":
                    description: OK response.
                    schema:
                        $ref: '#/definitions/SignerVerificationMethodResponseBody'
                        required:
                            - id
                            - type
                            - controller
                            - publicKeyJwk
            schemes:
                - http
definitions:
    DIDVerificationMethodResponse:
        title: DIDVerificationMethodResponse
        type: object
        properties:
            controller:
                type: string
                description: Controller of verification method specified as DID.
                example: did:web:example.com
            id:
                type: string
                description: ID of verification method.
                example: key1
            publicKeyJwk:
                type: string
                description: Public Key encoded in JWK format.
                example: Illum qui nihil.
                format: binary
            type:
                type: string
                description: Type of verification method key.
                example: JsonWebKey2020
        example:
            controller: did:web:example.com
            id: key1
            publicKeyJwk: Ut occaecati repellat.
            type: JsonWebKey2020
        required:
            - id
            - type
            - controller
            - publicKeyJwk
    SignerCreatePresentationRequestBody:
        title: SignerCreatePresentationRequestBody
        type: object
        properties:
            context:
                type: array
                items:
                    type: string
                    example: Et nulla illo totam optio quia ab.
                description: Additional JSONLD contexts to be specified in the VP.
                example:
                    - https://w3id.org/security/suites/jws-2020/v1
                    - https://schema.org
            data:
                type: array
                items:
                    type: string
                    example: Natus quos ut corrupti.
                    format: binary
                description: Raw JSON to be included inside the VP as Verifiable Credential.
                example:
                    - hello: world
                    - hola: mundo
            issuer:
                type: string
                description: Issuer DID of the Verifiable Presentation.
                example: did:web:example.com
            key:
                type: string
                description: Key to use for the proof signature.
                example: key1
            namespace:
                type: string
                description: Key namespace.
                example: transit
        example:
            context:
                - https://w3id.org/security/suites/jws-2020/v1
                - https://schema.org
            data:
                - hello: world
                - hola: mundo
            issuer: did:web:example.com
            key: key1
            namespace: transit
        required:
            - issuer
            - namespace
            - key
            - data
    SignerCredentialProofRequestBody:
        title: SignerCredentialProofRequestBody
        type: object
        properties:
            credential:
                type: string
                description: Verifiable Credential in JSON format.
                example:
                    context:
                        - https://www.w3.org/2018/credentials/v1
                        - https://w3id.org/security/suites/jws-2020/v1
                        - https://schema.org
                    type: VerifiableCredential
                    issuer: did:web:nginx:policy:policy:example:example:1.0:evaluation
                    issuancedate: 2010-01-01T19:23:24.651387237Z
                    credentialsubject:
                        name: Alice
                        allow: true
                format: binary
            key:
                type: string
                description: Key to use for the proof signature (optional).
                example: key1
            namespace:
                type: string
                description: Key namespace.
                example: transit
        example:
            credential:
                context:
                    - https://www.w3.org/2018/credentials/v1
                    - https://w3id.org/security/suites/jws-2020/v1
                    - https://schema.org
                type: VerifiableCredential
                issuer: did:web:nginx:policy:policy:example:example:1.0:evaluation
                issuancedate: 2010-01-01T19:23:24.651387237Z
                credentialsubject:
                    name: Alice
                    allow: true
            key: key1
            namespace: transit
        required:
            - namespace
            - key
            - credential
    SignerPresentationProofRequestBody:
        title: SignerPresentationProofRequestBody
        type: object
        properties:
            issuer:
                type: string
                description: Issuer DID used to specify proof verification info.
                example: Non amet.
            key:
                type: string
                description: Key to use for the proof signature.
                example: key1
            namespace:
                type: string
                description: Key namespace.
                example: transit
            presentation:
                type: string
                description: Verifiable Presentation in JSON format.
                example: Nihil iste debitis.
                format: binary
        example:
            issuer: Sapiente dolorem qui possimus qui labore veritatis.
            key: key1
            namespace: transit
            presentation: Fuga officia excepturi velit aut.
        required:
            - issuer
            - namespace
            - key
            - presentation
    SignerVerificationMethodResponseBody:
        title: SignerVerificationMethodResponseBody
        type: object
        properties:
            controller:
                type: string
                description: Controller of verification method specified as DID.
                example: did:web:example.com
            id:
                type: string
                description: ID of verification method.
                example: key1
            publicKeyJwk:
                type: string
                description: Public Key encoded in JWK format.
                example: Accusamus dolor illo aliquid ipsum optio.
                format: binary
            type:
                type: string
                description: Type of verification method key.
                example: JsonWebKey2020
        description: Public Key represented as DID Verification Method.
        example:
            controller: did:web:example.com
            id: key1
            publicKeyJwk: Et et cupiditate debitis.
            type: JsonWebKey2020
        required:
            - id
            - type
            - controller
            - publicKeyJwk
    SignerVerifyCredentialResponseBody:
        title: SignerVerifyCredentialResponseBody
        type: object
        properties:
            valid:
                type: boolean
                description: Valid specifies if the proof is successfully verified.
                example: false
        example:
            valid: true
        required:
            - valid
    SignerVerifyPresentationResponseBody:
        title: SignerVerifyPresentationResponseBody
        type: object
        properties:
            valid:
                type: boolean
                description: Valid specifies if the proof is successfully verified.
                example: true
        example:
            valid: false
        required:
            - valid
